from linked_list import LinkedList
from linked_list import Node


if __name__ == '__main__':
    lst = LinkedList()

    lst.head = Node(3)
    lst.insert_at_the_begging(2)
    lst.insert_at_the_begging(1)
    lst.insert_at_the_begging(0)
    lst.insert_in_the_end(4)


    lst.print_list()
